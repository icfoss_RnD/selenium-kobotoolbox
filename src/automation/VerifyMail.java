package automation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import java.io.FileReader;
import au.com.bytecode.opencsv.CSVReader;
import java.util.concurrent.TimeUnit;

public class VerifyMail {

	private static final String VERIFY_FILE_PATH = "/home/user/apps/life/kobo/xls_upload/mail_verification_list.txt";
		
	public VerifyMail(){
		//Add user page parameters
	}
	
	public void readURL(WebDriver driver) throws Exception{

		CSVReader reader = new CSVReader(new FileReader(VERIFY_FILE_PATH));
		String [] nextLine;
		while ((nextLine = reader.readNext()) != null) {
			try{
			String REGUSER_URL = nextLine[0];
			driver.get(REGUSER_URL);
			TimeUnit.SECONDS.sleep(5);	//5 second delay
//			driver.navigate().refresh(); //refresh page
			System.out.println(REGUSER_URL);
			} catch(Exception e){
				System.out.println(e.getMessage());
				TimeUnit.SECONDS.sleep(5);	//5 second delay
				driver.navigate().refresh(); //refresh page
			}
		}
		reader.close();
	}
		
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		// geckodriver for firefox
//		System.setProperty("webdriver.gecko.driver", "/home/user/bin/geckodriver");
		//chromedriver
		System.setProperty("webdriver.chrome.driver", "/home/user/bin/chromedriver");
		//driver firefox
//		WebDriver driver=new FirefoxDriver();
		ChromeOptions options = new ChromeOptions();
//		options.addArguments("--incognito");
		//driver chrome
		WebDriver driver=new ChromeDriver(options);
		//waiting implicitly for each page loading
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		VerifyMail obj = new VerifyMail();
		obj.readURL(driver);
	}

}

